//
//  ViewController.swift
//  homework3
//
//  Created by student on 10/16/18.
//  Copyright © 2018 MZ. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
        override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        //printMyName()
        //checkSex(surname: "Zorianovich")
        //checkSex(surname: "Харитон")
        //checkSex(surname: "Ульяновна")
        //print(splitFullName(fullName: "MyroslavZoryanovich") ?? "")
        //let s = "улыбок пипи дед мокар"
        //print("\(s)\n reverse is\n \(reverseString(param: s))")
        //print("\(markValueWithCommas(value: 5431234567))")
        //let password = "fgdmhh!gL1sah"
        //print("the safety of password \(password) is \(checkPasswordStrength(password: password))")
        //let array = [7.6, 3, 2, 6.5, 4, 8.0, 8, 88,8,8,8,8,6,6,6,6,6,43,43,4,44,4,4,3, 3.1, 6, 6.5, -4, 0, 5.23]
        //print("array \(array) sorted without duplicates\n \(sortArray(array: array))")
//        let s = "грязНые Люди с пОРазительно чистой Судьбой!"
//        print("\(s) transliterated - \(translitText(text: s))")
//        let data = searchSubstring(text: ["Грабляебля", "шоБля","спибля","нема","йдіть до сраки","Шабля"], searchFor: "бЛя")
//        print("\(data)")
        let fucks = Set.init(["хуй", "пизд", "хуи", "хуя", "хуе", "хуё", "еба", "ёба", "еби", "еб ", "блядь", "блять", "бляц", "бляд"])
            print("\(noFuckNoSuck(text: ["А я день", "рожденья не", "буду справлять!", "Всё заебало", "пиздец", "нахуй", "блять!", "извиниите, был напуган","Если мы будем не делать а думать так и останемся", " пиздоболами", "ненунахуяты", "блядство это скверно", "ЕСТЬ ПОСТУПКИ БЛЯЦКИЕ" ], setOfFucks: fucks))")
    }
    
    func printMyName() {
        let name = "Myroslav"
        print("my name is \(name)\n the count of letters in my name is \(name.count)")
    }
    
    func checkSex(surname: String) {
        if surname.hasSuffix("ich") || surname.hasSuffix("ичь") {
            print("\(surname) is a male gender")
            return
        }
        
        if surname.hasSuffix("na") || surname.hasSuffix("на") {
            print("\(surname) is a female gender")
        }
        else {
            print("\(surname) is not something completely clear. I don't get it...")
        }
    }
    
    func splitFullName(fullName: String) -> String? {
        let surnameIndex = fullName.index(of:"Z") ?? fullName.endIndex
        
        if surnameIndex == fullName.endIndex {
            print("That's not my full name")
            return nil
        }
        
        return String(fullName[..<surnameIndex]) + " " + String(fullName[surnameIndex..<fullName.endIndex])
    }
    
    func reverseString(param: String) -> String {
        var result = ""
        
        for i in 1...param.count {
            result.append(param[param.index(param.startIndex, offsetBy: param.count - i)])
        }
        
        return result
    }
    
    func markValueWithCommas(value: Int) -> String {
        var result = String(value)
        let rest = result.count % 3
        let commasCount: Int = result.count / 3
        
        for i in 0..<commasCount {
            result.insert(",", at: result.index(result.startIndex, offsetBy: rest + i * 3 + i))
        }
        
        return result
    }
    
    func containsDigit(string: String) -> Bool {
        let digits: Set = ["0","1","2","3","4","5","6","7","8","9"]
        var result = false
        
        for cha in digits {
            if string.contains(cha) {
                result = true
                break
            }
        }
        
        return result
    }
    
    func containsSpecialSymbols(string: String) -> Bool {
        return string.rangeOfCharacter(from: CharacterSet.alphanumerics.inverted) != nil
    }
    
    func checkPasswordStrength(password: String) -> Int {
        var result = 0
        
        if containsDigit(string: password) {
            result += 1
            print("string \(password) contains digits +1")
        }
        
        if password.lowercased() != password {
            result += 1
            print("string \(password) contains upper case symbols +1")
        }
        
        if password.uppercased() != password {
            result += 1
            print("string \(password) contains lower case symbols +1")
        }
        
        if containsSpecialSymbols(string: password) {
            result += 1
            print("string \(password) contains special symbols +1")
        }
        
        if result == 4 {
            print("password is completely safe")
        }
        
        return result
    }
    
    func removeDuplicates(array: [Double]) -> [Double] {
        let result = Array(Set<Double>(array))
        
        // or we can do like this
        
        /*var i = 0
         var j = 0
         
         while i < result.count  {
         j = i + 1
         
         while j < result.count {
         if result[i] == result[j] {
         result.remove(at: j)
         continue
         }
         
         j += 1
         }
         
         i += 1
         }*/
        
        return result
    }
    
    func sortArray(array: [Double]) -> [Double] {
        var result = array
        
        result = removeDuplicates(array: result)
        
        var badOrder = true
        
        while badOrder {
            badOrder = false
            
            for i in 1..<result.count {
                if result[i - 1] > result[i] {
                    badOrder = true
                    let tmp = result[i]
                    result[i] = result[i - 1]
                    result[i - 1] = tmp
                }
            }
        }
        
        return result
    }
    
    func translitText(text: String) -> String {
        var result: String = ""
        let translitDict: Dictionary<String, String> = [
            "А" : "A",
            "Б" : "B",
            "В" : "V",
            "Г" : "G",
            "Д" : "D",
            "Е" : "E",
            "Ё" : "JO",
            "Ж" : "ZH",
            "З" : "Z",
            "И" : "I",
            "Й" : "J",
            "К" : "K",
            "Л" : "L",
            "М" : "M",
            "Н" : "N",
            "О" : "O",
            "П" : "P",
            "Р" : "R",
            "С" : "S",
            "Т" : "T",
            "У" : "U",
            "Ф" : "F",
            "Х" : "H",
            "Ц" : "TS",
            "Ч" : "CH",
            "Ш" : "SH",
            "Щ" : "SHCH",
            "Ъ" : "",
            "Ы" : "Y",
            "Ь" : "'",
            "Э" : "E",
            "Ю" : "JU",
            "Я" : "JA"
        ]
        
        for (_, char) in text.enumerated() {
            if String(char).rangeOfCharacter(from: CharacterSet.alphanumerics) == nil {
                result.append(char)
                continue
            }
            
            if String(char).uppercased() != String(char) {
                result += (translitDict[String(char).uppercased()] ?? String(char)).lowercased()
            }
            else {
                result += translitDict[String(char)] ?? String(char)            }
        }
        
        return result
    }
    
    func noCaseSymbolFirstIndex(word: String, symbol: Character) -> String.Index {
        return word.firstIndex(of: symbol) ??
            word.firstIndex(of: Character(String(symbol).uppercased())) ??
                word.endIndex
    }
    
    func searchSubstring(text: [String], searchFor: String, hideFound: Bool = false) -> [String] {
        var result = [String].init()

        for s in text {
            var word = s
            var maskedWord = ""
            var isWordToPick = false
            var index1 = word.startIndex
            let sym = searchFor[searchFor.startIndex]

            while noCaseSymbolFirstIndex(word: word, symbol: sym) != word.endIndex && index1 != word.endIndex {
                var phrase = String(sym)
                
                index1 = word.index(after: noCaseSymbolFirstIndex(word: word, symbol: sym))
                
                var index2 = searchFor.index(after: searchFor.startIndex)
                
                while index1 != word.endIndex && index2 != searchFor.endIndex {
                    if String(word[index1]).uppercased() != String(searchFor[index2]).uppercased() {
                        maskedWord += String(word[word.startIndex..<index1])
                        word = String(word[index1...])
                        break
                    }
                    
                    phrase += String(word[index1])
                    index1 = word.index(after: index1)
                    index2 = searchFor.index(after: index2)
                }
                
                if String(phrase).uppercased() == String(searchFor).uppercased() {
                    isWordToPick = true
                    
                    if hideFound {
                        word.replaceSubrange(noCaseSymbolFirstIndex(word: word, symbol: sym)..<index1, with: Array.init(repeating: "*", count: searchFor.count))
                    }
                    
                    maskedWord += String(word[word.startIndex..<index1])
                    word = String(word[index1...])
                }
                
                if noCaseSymbolFirstIndex(word: word, symbol: sym) == word.endIndex {
                    maskedWord += word
                }
            }
            
            if isWordToPick {
                result.append(maskedWord)
            }
        }
        
        return result
    }
    
    func noFuckNoSuck(text: [String], setOfFucks: Set<String>) -> [String] {
        var cleanText: [String] = Array.init(text)
        
        print(text)
        
        for fuck in setOfFucks {
            let fuckWords = searchSubstring(text: text, searchFor: fuck, hideFound: false)
            
            for (i, s) in text.enumerated() {
                
                if fuckWords.contains(s) {
                    cleanText[i] = searchSubstring(text: Array.init(repeating: s, count: 1), searchFor: fuck, hideFound: true).joined()
                }
            }
        }
        
        return cleanText
    }
}



